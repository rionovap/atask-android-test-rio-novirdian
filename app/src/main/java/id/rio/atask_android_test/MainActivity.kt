package id.rio.atask_android_test

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import androidx.databinding.DataBindingUtil
import androidx.exifinterface.media.ExifInterface
import com.canhub.cropper.CropImageContract
import com.canhub.cropper.CropImageContractOptions
import com.canhub.cropper.CropImageView
import com.canhub.cropper.options
import id.rio.atask_android_test.databinding.ActivityMainBinding
import id.rio.atask_android_test.utils.DialogPermissionRequest
import id.rio.atask_android_test.utils.Globals
import id.rio.atask_android_test.utils.OcrKitUtil
import id.rio.atask_android_test.utils.PermissionUtils
import id.rio.atask_android_test.utils.ProgressDialogView
import java.io.File
import java.io.FileNotFoundException
import java.io.IOException
import java.util.Calendar


class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var cropImageActivityLauncher: ActivityResultLauncher<CropImageContractOptions>
    private lateinit var filePermissionLauncher: ActivityResultLauncher<Array<String>>
    private lateinit var cameraActivityLauncher: ActivityResultLauncher<Intent>
    private lateinit var galleryActivityLauncher: ActivityResultLauncher<Intent>
    private var capturedImageUri: Uri? = null
    private var fileToUpload: File? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.viewModel = MainViewModel(this@MainActivity.application)
        initView()
    }

    private fun initView (){
        observeFunctionType()
        observeScanValidityStatus()
        observeScanExpressionText()
        observeScanCalculationResult()
        observeScanLoading()

        filePermissionLauncher = registerForActivityResult(
            ActivityResultContracts.RequestMultiplePermissions()
        ){
            if (PermissionUtils.checkStoragePermission(this@MainActivity)) {
                if(binding.viewModel?.functionalityType?.value?.peekContent() == Globals.FILE_SYSTEM){
                    initPickFile()
                }else{
                    initCaptureImage()
                }
            } else {
                DialogPermissionRequest(this@MainActivity).init(getString(R.string.msg_permission_storage), false){

                }
            }
        }


        cameraActivityLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                try {
                    var rotate = 0
                    val fileImg = fileToUpload
                    val exif = ExifInterface(fileImg!!.absolutePath)
                    when (exif.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_NORMAL)) {
                        ExifInterface.ORIENTATION_ROTATE_270 -> rotate = 270
                        ExifInterface.ORIENTATION_ROTATE_180 -> rotate = 180
                        ExifInterface.ORIENTATION_ROTATE_90 -> rotate = 90
                    }
                    val matrix = Matrix()
                    matrix.postRotate(rotate.toFloat())
                    cropImageActivityLauncher.launch(options(capturedImageUri) {
                        setGuidelines(CropImageView.Guidelines.ON)
                        setOutputCompressFormat(Bitmap.CompressFormat.JPEG)
                    })
                } catch (e: FileNotFoundException) {
                    e.printStackTrace()
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
        }

        galleryActivityLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                try {
                    try {
                        cropImageActivityLauncher.launch(options(result.data?.data) {
                            setGuidelines(CropImageView.Guidelines.ON)
                            setOutputCompressFormat(Bitmap.CompressFormat.JPEG)
                        })
                    } catch (e: FileNotFoundException) {
                        e.printStackTrace()
                        Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show()
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                    Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show()
                }
            }
        }

        cropImageActivityLauncher = registerForActivityResult(CropImageContract()) { result ->
            if (result.isSuccessful) {
                val uriFilePath = result.getUriFilePath(this) // optional usage
                val bitmap = BitmapFactory.decodeFile(uriFilePath)
                val ocrKitUtil = OcrKitUtil()
                ocrKitUtil.doTextRecognizer(ocrKitUtil.imageFromBitmap(bitmap)){ scanSuccess, text ->
                    if(scanSuccess && text != null) {
                        binding.viewModel?.processScanResult(text)
                    }else{
                        Toast.makeText(this@MainActivity, "Input Gambar/teks tidak valid, coba lagi dengan gambar dan teks yang lebih jelas.", Toast.LENGTH_SHORT).show()
                    }
                }
            } else {
                Toast.makeText(this@MainActivity, result.error?.message.toString(), Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun observeFunctionType(){
        binding.viewModel?.functionalityType?.observe(this@MainActivity) {
            it.getContentIfNotHandled().let { functionType ->
                if(PermissionUtils.checkStoragePermission(this@MainActivity)){
                    if(functionType == Globals.FILE_SYSTEM){
                        Toast.makeText(this@MainActivity, "File", Toast.LENGTH_SHORT).show()
                        initPickFile()
                    }else{
                        Toast.makeText(this@MainActivity, "Camera", Toast.LENGTH_SHORT).show()
                        initCaptureImage()
                    }
                }else{
                    PermissionUtils.initStoragePermission(filePermissionLauncher)
                }
            }
        }
    }

    private fun observeScanLoading(){
        val progresDialogView = ProgressDialogView(this@MainActivity)
        var dialog = Dialog(this@MainActivity)
        binding.viewModel?.loadingProgressViewStatus?.observe(this@MainActivity){ isLoading ->
            if(isLoading){
                dialog = progresDialogView.showProgressDialog()
            }else{
                progresDialogView.closeProgressDialog(dialog)
            }
        }
    }

    private fun observeScanValidityStatus(){
        binding.viewModel?.scanValidityStatus?.observe(this@MainActivity) {
            it.getContentIfNotHandled()?.let { status ->
                if(!status){
                    binding.txtResult.text = ""
                    Toast.makeText(this@MainActivity, "Input Gambar/teks tidak valid, coba lagi dengan gambar dan teks yang lebih jelas.", Toast.LENGTH_SHORT).show()
                }else{
                    Toast.makeText(this@MainActivity, "Berhasil", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun observeScanExpressionText(){
       binding.viewModel?.viewExpressionText?.observe(this@MainActivity){
           binding.txtOperation.text = "Operation : $it"
       }
    }

    @SuppressLint("SetTextI18n")
    private fun observeScanCalculationResult(){
        binding.viewModel?.showCalculationResult?.observe(this@MainActivity){ result ->
            binding.txtResult.text = "Result : $result"
        }
    }




    private fun initCaptureImage() {
        val cal = Calendar.getInstance()
        fileToUpload = File(this.getExternalFilesDir(Environment.DIRECTORY_PICTURES), "ihc_telemed_file")
        if (!fileToUpload!!.exists()) {
            fileToUpload?.mkdirs()
        }
        fileToUpload = File(fileToUpload, cal.timeInMillis.toString() + ".jpg")
        capturedImageUri = FileProvider.getUriForFile(this,
            resources.getString(R.string.atask_android_content_provider),
            fileToUpload!!)

        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        intent.putExtra(MediaStore.EXTRA_OUTPUT, capturedImageUri)
        cameraActivityLauncher.launch(intent)
    }

    private fun initPickFile() {
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        galleryActivityLauncher.launch(intent)
    }




}