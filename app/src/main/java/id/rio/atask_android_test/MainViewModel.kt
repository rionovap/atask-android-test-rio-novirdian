package id.rio.atask_android_test

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.google.mlkit.vision.text.Text
import id.rio.atask_android_test.utils.EventHandler
import id.rio.atask_android_test.utils.Globals
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import java.lang.StringBuilder
import java.util.Hashtable
import java.util.regex.Pattern

class MainViewModel(application: Application) : AndroidViewModel(application) {

    private val onClickEventFunctionalityType = MutableLiveData<EventHandler<String>>()
    private val onProcessCalculation = MutableLiveData<String>()
    private val onScanValidation = MutableLiveData<EventHandler<Boolean>>()
    private val onLoadingView = MutableLiveData<Boolean>()
    private val onViewExpressionText = MutableLiveData<String>()
    val functionalityType: LiveData<EventHandler<String>> = onClickEventFunctionalityType
    val scanValidityStatus: LiveData<EventHandler<Boolean>> = onScanValidation
    val loadingProgressViewStatus: LiveData<Boolean> = onLoadingView
    val showCalculationResult: LiveData<String> = onProcessCalculation
    val viewExpressionText: LiveData<String> = onViewExpressionText

    fun onClickScan() {
        if (BuildConfig.FUNCTION_TYPE == Globals.FILE_SYSTEM) {
            onClickEventFunctionalityType.value = EventHandler(Globals.FILE_SYSTEM)
        } else {
            onClickEventFunctionalityType.value = EventHandler(Globals.CAMERA)
        }
    }

    fun processScanResult(scanResult: Text) {
        var founded = false
        var operation = ""
        viewModelScope.launch {

            onLoadingView.value = true
            try {
                val job = async {
                    for (n: Int in 0 until scanResult.textBlocks.size) {
                        val pattern = Pattern.compile(Globals.ARITHMETIC_REGEX)
                        val matcher = pattern.matcher(scanResult.textBlocks[n].text)
                        founded = matcher.find()
                        if (founded) {
                            operation = matcher.group(0)!!
                                .replace(" ", "")
                                .replace(",", ".")
                                .replace("x", "*")
                                .replace("X", "*")

                            break
                        }
                    }
                }
                job.await()

                onViewExpressionText.value = operation
                onProcessCalculation.value = processText(operation)
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                onLoadingView.value = false
                onScanValidation.value = EventHandler(founded)
            }
        }

    }


    private fun processText(operationText: String): String {

        val leftOperand = StringBuilder()
        val rightOperand = StringBuilder()
        var operator = ""

        for (n: Int in operationText.indices) {

            if ((operationText[n].isDigit() || operationText[n] == '.') && operator.isEmpty()) {
                leftOperand.append(operationText[n])
            }else if (!operationText[n].isDigit() && operator.isEmpty()) {
                operator = operationText[n].toString()
            } else{
                rightOperand.append(operationText[n].toString())
            }

        }

        return operate(operator, leftOperand, rightOperand).toString()
    }

    private fun operate(operator: String, leftOperand: Any, rightOperand: Any): Any{
        val opsHastTable = Hashtable<String, Double>()
        opsHastTable["-"] = (leftOperand).toString().toDouble() - rightOperand.toString().toDouble()
        opsHastTable["+"] = (leftOperand).toString().toDouble() + rightOperand.toString().toDouble()
        opsHastTable["/"] = (leftOperand).toString().toDouble() / rightOperand.toString().toDouble()
        opsHastTable["*"] = (leftOperand).toString().toDouble() * rightOperand.toString().toDouble()

        return opsHastTable[operator]!!
    }
}