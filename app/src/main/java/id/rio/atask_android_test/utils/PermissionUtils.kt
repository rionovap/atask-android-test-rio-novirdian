package id.rio.atask_android_test.utils

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import androidx.activity.result.ActivityResultLauncher
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat

object PermissionUtils {

    const val PERMISSION_REQ_STORAGE = 22


    fun initStoragePermission(activity: Activity) {
        val permissionReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE
        val permissioWriteStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE
        val permissionCamera = Manifest.permission.CAMERA
        val grantReadStorage = ContextCompat.checkSelfPermission(activity, permissionReadStorage)
        val grantWriteStorage = ContextCompat.checkSelfPermission(activity, permissioWriteStorage)
        val grantCamera = ContextCompat.checkSelfPermission(activity, permissioWriteStorage)
        val permissionList = arrayOfNulls<String>(3)
        permissionList[0] = permissionReadStorage
        permissionList[1] = permissioWriteStorage
        permissionList[2] = permissionCamera
        if (grantReadStorage != PackageManager.PERMISSION_GRANTED ||
            grantWriteStorage != PackageManager.PERMISSION_GRANTED ||
            grantCamera != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(activity, permissionList, PERMISSION_REQ_STORAGE)
        }
    }

    fun initStoragePermission(launcher: ActivityResultLauncher<Array<String>>) {
        val permissionReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE
        val permissioWriteStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE
        val permissionCamera = Manifest.permission.CAMERA
        launcher.launch(arrayOf(permissionReadStorage, permissioWriteStorage, permissionCamera))
    }

    fun checkStoragePermission(context: Context): Boolean{
        val permissionReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE
        val permissionWriteStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE
        val permissionCamera = Manifest.permission.CAMERA
        val grantReadStorage = ContextCompat.checkSelfPermission(context, permissionReadStorage)
        val grantWriteStorage = ContextCompat.checkSelfPermission(context, permissionWriteStorage)
        val grantCamera = ContextCompat.checkSelfPermission(context, permissionCamera)
        val permissionList = arrayOfNulls<String>(3)
        permissionList[0] = permissionReadStorage
        permissionList[1] = permissionWriteStorage
        permissionList[2] = permissionCamera
        return !(grantReadStorage != PackageManager.PERMISSION_GRANTED ||
                grantWriteStorage != PackageManager.PERMISSION_GRANTED ||
                grantCamera != PackageManager.PERMISSION_GRANTED)
    }

}