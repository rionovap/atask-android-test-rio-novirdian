package id.rio.atask_android_test.utils

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.net.Uri
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.databinding.DataBindingUtil
import id.rio.atask_android_test.R
import id.rio.atask_android_test.databinding.PermissionRequestDialogBinding

class DialogPermissionRequest(var activity: Activity) : Dialog(activity) {


    fun init(settingMsg: String, cancelable: Boolean = false, callback: (Boolean) -> Unit){
        val binding =  DataBindingUtil.inflate<PermissionRequestDialogBinding>(LayoutInflater.from(activity), R.layout.permission_request_dialog, null, false)
        setContentView(binding.root)
        window?.setWindowAnimations(R.style.DialogAnimationFromBottom)
        window?.setBackgroundDrawableResource(R.drawable.bg_dialog_rounded_white)
        window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        setCancelable(cancelable)
        setCanceledOnTouchOutside(cancelable)
        setOnCancelListener {

        }
        binding.txtPermissiionMsge.text = settingMsg

        if(cancelable){
            binding.btnNo.setOnClickListener {
                callback(true)
                dismiss()
            }
        }else{
            binding.btnNo.visibility = View.GONE
        }

        binding.btnGotoSetting.setOnClickListener {
            val i = Intent()
            i.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
            i.addCategory(Intent.CATEGORY_DEFAULT)
            i.data = Uri.parse("package:" + activity.packageName)
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
            i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS)
            activity.startActivity(i)
            callback(false)
            dismiss()
        }

        show()
    }
    
}