package id.rio.atask_android_test.utils

object Globals {
    val FILE_SYSTEM = "FILE_SYSTEM"
    val CAMERA = "CAMERA"
    val ARITHMETIC_REGEX = "(^\\s*([0-9]+|[0-9]+([,]|[.])+[0-9]+)+\\s*([/]|[+]|[-]|[*]|[x]|[X])\\s*([0-9]+|[0-9]+([,]|[.])+[0-9]+)\\s*)\$"
}